package estswitchyfor;

import java.util.Scanner;

public class EstSwitchYFor {
   
    // Imprime el mensaje y salta una línea
    static void imprimirMsjL(String sMsj){
        System.out.println(sMsj);
    }
    
    // Imprime el mensaje sin saltar una línea
    static void imprimirMsjSinL(String sMsj){
        System.out.print(sMsj);
    }

    // Imprime un separador
    static void separador(){
        imprimirMsjL("--------------------------------------------------------------");
    }
    
    // Imprime información del programa y personal
    static void infoP(){
        imprimirMsjL("Estructuras Switch y For");
        imprimirMsjL("Uc Vazquez D. Carolina, 57618");
        separador();
    }
    
    // Menú con opciones posibles a realizar
    static void menu(){
        Scanner tec = new Scanner (System.in);
        imprimirMsjL("MENÚ - Elija la opción que desee entre las siguientes:\n");
        imprimirMsjL("1.- Trimestre del año en el que nos encontramos (Switch 5)");
        imprimirMsjL("2.- Calcular factorial de un número (For 3)");
        imprimirMsjL("3.- Modelo del juego Gato (For 8)");
        imprimirMsjL("4.- Finalizar programa");
        imprimirMsjSinL("\nIngrese el número de opción elegido: ");
        int iOpcion = tec.nextInt();
        submenu(iOpcion);
    }
    
    // Submenú que realiza la opción deseada
    static void submenu(int iOpcion){
        separador();
        switch (iOpcion){
            case 1:
                imprimirMsjL("-Trimestre del año en el que nos encontramos-");            
                switchNum5();
                separador();
                break;
            case 2:
                imprimirMsjL("-Calcular factorial de un número-");
                forNum3();
                separador();
                break;
            case 3:
                imprimirMsjL("-Modelo del juego Gato-");
                forNum8();
                separador();
                break;
            case 4:
                imprimirMsjL("¡Vuelva pronto!");
                separador();
                break;
            default: 
                imprimirMsjL("Opción inexistente");
                separador();
        }
    }
    
    // Switch 5 - Trimestre del año en el que nos encontramos
    static void switchNum5(){
        Scanner tec = new Scanner (System.in); 
        imprimirMsjSinL("Ingrese el número de mes actual (del 1 al 12): ");
        int iMes = tec.nextInt();
        separador();
        switch (iMes){
            case 1: case 2: case 3: imprimirMsjL("Nos encontramos en el Primer Trimestre del año"); 
                break;
            case 4: case 5: case 6: imprimirMsjL("Nos encontramos en el Segundo Trimestre del año");
                break;
            case 7: case 8: case 9: imprimirMsjL("Nos encontramos en el Tercer Trimestre del año");
                break;
            case 10: case 11: case 12: imprimirMsjL("Nos encontramos en el Cuarto Trimestre del año");
                break;
            default: imprimirMsjL("El número de mes ingresado no es válido");
        }
    }
    
    // For 3 - Calcular factorial de un número
    static void forNum3(){
        Scanner tec = new Scanner (System.in);
        imprimirMsjSinL("Ingrese un número para calcular su factorial: ");
        int iNum = tec.nextInt();
        separador();
        long iFact = 1;
        for (int i=1; i<=iNum; i++){
            iFact = iFact * i;
        }
        System.out.println("El factorial de " + iNum + " es " + iFact);
    }
    
    // For 8 - Modelo del juego Gato
    static void forNum8(){
        String[][] juegoGato = {{"0,0", "0,1", "0,2"}, {"1,0", "1,1", "1,2"}, {"2,0", "2,1", "2,2"}};
        imprimirMsjL("\n     0     1     2\n");
        for (int i=0; i<3; i++){
            System.out.print(i + "   ");
            for (int j=0; j<3; j++){
                System.out.print(juegoGato[i][j]);
                if (j<2) imprimirMsjSinL(" | ");
            }
            if (i<2) imprimirMsjL("\n    ---------------");
                else imprimirMsjL("\n");
        }
    }
    
    // Run
    public static void main(String[] args) {
        infoP();
        menu();
        infoP();
    }
    
}